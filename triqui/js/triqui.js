DifficultyLevel = {
    Easy: 0,
    Harder: 1,
    Expert: 2
};

const boardSize = 9;
const humanMark = 'X';
const computerMark = 'O';
const openSpot = '';
let board = [openSpot, openSpot, openSpot, openSpot, openSpot, openSpot, openSpot, openSpot, openSpot];
let moves = 0;
let level = DifficultyLevel.Expert;

$('.alert').alert();

$(".field").click(function () {
    if ($(this).text() !== '')
        return;

    setMove(humanMark, $(this).attr('id'));
});

$('button').click(function () {
    if ($(this).attr('id') === 'Easy')
        level = DifficultyLevel.Easy;
    if ($(this).attr('id') === 'Harder')
        level = DifficultyLevel.Harder;
    if ($(this).attr('id') === 'Expert')
        level = DifficultyLevel.Expert;
    clearBoard();
});

function randomInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

function clearBoard() {
    $('.field').text(openSpot);
    for (var i = 0; i < boardSize; i++) {
        board[i] = openSpot;
    }
}

function checkForWinner() {
    for (var i = 0; i <= 6; i += 3) {
        if (board[i] == humanMark &&
            board[i + 1] == humanMark &&
            board[i + 2] == humanMark)
            return 2;

        if (board[i] == computerMark &&
            board[i + 1] == computerMark &&
            board[i + 2] == computerMark)
            return 3;
    }

    for (var i = 0; i <= 2; i++) {
        if (board[i] == humanMark &&
            board[i + 3] == humanMark &&
            board[i + 6] == humanMark)
            return 2;

        if (board[i] == computerMark &&
            board[i + 3] == computerMark &&
            board[i + 6] == computerMark)
            return 3;
    }

    if ((board[0] == humanMark &&
        board[4] == humanMark &&
        board[8] == humanMark) ||
        (board[2] == humanMark &&
            board[4] == humanMark &&
            board[6] == humanMark))
        return 2;

    if ((board[0] == computerMark &&
        board[4] == computerMark &&
        board[8] == computerMark) ||
        (board[2] == computerMark &&
            board[4] == computerMark &&
            board[6] == computerMark))
        return 3;

    for (var i = 0; i < boardSize; i++) {
        if (board[i] != humanMark && board[i] != computerMark)
            return 0;
    }
    return 1;
}

function getComputerMove() {
    if (moves === 9) return;
    var move;
    if (level == DifficultyLevel.Easy) {
        do {
            move = randomInRange(0, 8);
        } while (board[move] == humanMark || board[move] == computerMark);
    } else if (level == DifficultyLevel.Harder) {
        for (var i = 0; i < boardSize; i++) {
            if (board[i] != humanMark && board[i] != computerMark) {
                var curr = board[i];
                board[i] = computerMark;
                if (checkForWinner() == 3) {
                    board[i] = openSpot;
                    return i;
                }
                else
                    board[i] = curr;
            }
        }

        do {
            move = randomInRange(0, 8);
        } while (board[move] == humanMark || board[move] == computerMark);
    } else {
        for (var i = 0; i < boardSize; i++) {
            if (board[i] != humanMark && board[i] != computerMark) {
                var curr = board[i];
                board[i] = computerMark;
                if (checkForWinner() == 3) {
                    board[i] = openSpot;
                    return i;
                }
                else {
                    board[i] = curr;
                }
            }
        }

        for (var i = 0; i < boardSize; i++) {
            if (board[i] != humanMark && board[i] != computerMark) {
                var curr = board[i];
                board[i] = humanMark;
                if (checkForWinner() == 2) {
                    board[i] = openSpot;
                    console.log(setMove(computerMark, i));
                    console.log("Computer is moving to " + (i + 1));
                    return i;
                }
                else
                    board[i] = curr;
            }
        }

        do {
            move = randomInRange(0, 8);
        } while (board[move] == humanMark || board[move] == computerMark);
    }

    console.log("Computer is moving to " + (move + 1));
    setMove(computerMark, move);
    return move;
}

function setMove(player, location) {
    if (board[location] == openSpot) {
        moves++;
        if (player == humanMark) {
            board[location] = humanMark;
            if (checkForWinner() === 2) {
                $('#win-alert').addClass('show');
                moves = 9;
            } else {
                getComputerMove();
                if (checkForWinner() === 3) {
                    $('#lose-alert').addClass('show');
                    moves = 9;
                }
            }
            if (moves === 9) {
                $('#tie-alert').addClass('show');
            }
        } else {
            board[location] = computerMark;
        }
        $(`#${location}`).text(player);
        return true;
    }
    return false;
}